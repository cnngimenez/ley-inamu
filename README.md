Un ejemplo que muestra la modificación de la Ley 26.801.

El texto de la Ley 26.801 original fue obtenido desde http://www.infoleg.gob.ar/ y procesado para obtener el texto plano con el programa pandoc.

La Ley 26.801 original al día de la fecha (8 de febrero del 2024) se encuentra en la versión (commit) c5359d5 ([ver diff](https://gitlab.com/cnngimenez/ley-inamu/-/commit/c5359d5ef056b969d398751f4b1e20c477843830#5acee11c80db9a0714941abb0ab607969b383d37_166_166))

La Ley 26.801 modificada por el proyecto de ley se encuentra en la versión 
c5359d5e ([ver diff](https://gitlab.com/cnngimenez/ley-inamu/-/commit/c5359d5ef056b969d398751f4b1e20c477843830#5acee11c80db9a0714941abb0ab607969b383d37_166_166)).

Al hacer clic en ver diff, se observa la interfaz de GitLab para mostrar las diferencias entre una versión u otra.
